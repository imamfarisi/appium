package com.imamfarisi.appium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class App {

	public static void main(String[] args) {

		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability("deviceName", "LG K10 LTE");
		dc.setCapability("platformName", "Android");
		dc.setCapability("platformVersion", "6");
		dc.setCapability("app", "app-release.apk");
		dc.setCapability("appPackage", "com.imamfarisi.test");
		dc.setCapability("appActivity", "MainActivity");
		dc.setCapability("automationName", "UiAutomator2");

		try {
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			AppiumDriver<MobileElement> ap = new AppiumDriver<MobileElement>(url, dc);
			MobileElement a = ap.findElement(By.id("com.imamfarisi.test:id/navHome"));
			a.click();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
}
